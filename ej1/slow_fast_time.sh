#!/bin/bash

# inicializar variables
#Ninicio=500
Npaso=64
Nveces=5
#Nfinal=$((Ninicio + 400))
fDAT=time_slow_fast.dat
fPNG=time_slow_fast.png

P=$((1318%10))
Ninicio=$((10000+1024*$P))
Nfinal=$((10000+1024*($P+1)))

# borrar el fichero DAT y el fichero PNG
rm -f $fDAT fPNG

# generar el fichero DAT vacío
touch $fDAT

echo "Running slow and fast..."
# bucle para N desde P hasta Q 
#for N in $(seq $Ninicio $Npaso $Nfinal);
for (( i = 0 ; i < Nveces; i++));
do
	for (( N = Ninicio ; N <= Nfinal ; N += Npaso ))
	do
		echo "N: $N / $Nfinal..."
		
		# ejecutar los programas slow y fast consecutivamente con tamaño de matriz N
		# para cada uno, filtrar la línea que contiene el tiempo y seleccionar la
		# tercera columna (el valor del tiempo). Dejar los valores en variables
		# para poder imprimirlos en la misma línea del fichero de datos
		slowTime="$(./slow $N | grep 'time' | awk '{print $3}')"
		fastTime="$(./fast $N | grep 'time' | awk '{print $3}')"

		echo "$N	$slowTime	$fastTime" >> $fDAT
	done
done

touch temp
#agrupamos valores por primera columna y hallamos la media
awk '{
        slow[$1]   += $2;
        fast[$1]   += $3;
        sum[$1]    += 1;
    }
    END{
        for (t in slow) {
            print t " " slow[t]/sum[t] " " fast[t]/sum[t] >> "temp";
        }
    }' $fDAT

rm $fDAT
sort temp -o $fDAT

echo "Generating plot..."
# llamar a gnuplot para generar el gráfico y pasarle directamente por la entrada
# estándar el script que está entre "<< END_GNUPLOT" y "END_GNUPLOT"
gnuplot << END_GNUPLOT
set title "Slow-Fast Execution Time"
set ylabel "Execution time (s)"
set xlabel "Matrix Size"
set key right bottom
set grid
set term png
set output "$fPNG"
plot "$fDAT" using 1:2 with lines lw 2 title "slow", \
     "$fDAT" using 1:3 with lines lw 2 title "fast"
replot
quit
END_GNUPLOT
