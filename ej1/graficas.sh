#!/usr/bin/gnuplot
#set grid

set xlabel "Tamaño de matriz"
set ylabel "Tiempo de ejecución (s)"

set terminal pngcairo enhanced font "Liberation Sans,10"

set title "Slow-Fast Tiempo de ejecución"
set output "time_slow_fast.png"

set encoding "utf8"
set terminal pngcairo enhanced font "Liberation Sans,10"


plot "time_slow_fast.dat" using 1:2 with lines title "./slow", \
	 "time_slow_fast.dat" using 1:3 with lines title "./fast"

set terminal postscript eps size 5,3.5 enhanced color \
    font ',14' linewidth 1 
set output "time_slow_fast.eps"

replot

quit
