#!/bin/bash


P=$((1318%10))

nveces=10

MIN=$((10000+1024*$P))
MAX=$((10000+1024*($P+1)))

rm -f temp time_slow_fast.dat

for (( n=0; n<nveces; n++))
do
	N=$MIN

	while (( $N<=$MAX ))
	do
		# Sacamos los tiempos de manera intercalada 
		# para el tamaño N y para N+64

		echo -n "S "$N" "
		n1_slow="$(./slow $N | grep "Execution time:" | awk '{print $3}')"
		echo -n "S "$((N+64))" "
		n2_slow="$(./slow $((N+64)) | grep "Execution time:" | awk '{print $3}')"
		echo -n "F "$N" "
		n1_fast="$(./fast $N | grep "Execution time:" | awk '{print $3}')"
		echo -n "F "$((N+64))" "
		n2_fast="$(./fast $((N+64)) | grep "Execution time:" | awk '{print $3}')"
		
		# Imprimimos los datos ordenados en el fichero de salida 
		echo $N" "$n1_slow" "$n1_fast >> temp
		echo $((N+64))" "$n2_slow" "$n2_fast >> temp

		N=$(($N+128))

	done

done

# Agrupamos valores por primera columna y hallamos la media

awk '{
        slow[$1]   += $2;
        fast[$1]   += $3;
        sum[$1]    += 1;
    }
    END{
        for (t in slow) {
            print t " " slow[t]/sum[t] " " fast[t]/sum[t] >> "time_slow_fast.dat";
        }
    }' temp

rm temp

sort time_slow_fast.dat -o time_slow_fast.dat

echo "Generating plot..."
gnuplot << END_GNUPLOT
set grid

set xlabel "Matrix Size"
set ylabel "Execution time (s)"

set terminal pngcairo enhanced font "Liberation Sans,10"

set title "Slow-Fast Execution Time"
set output "time_slow_fast.png"

plot "time_slow_fast.dat" using 1:2 with lines title "./slow", \
	 "time_slow_fast.dat" using 1:3 with lines title "./fast"
quit
END_GNUPLOT

echo $P;
