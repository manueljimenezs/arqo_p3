#!/usr/bin/gnuplot

#set grid

set xlabel "Tamaño de matriz"
set ylabel "Número de fallos de lectura"

#set terminal pngcairo enhanced font "Liberation Sans,10"
set terminal postscript eps size 5,3.5 enhanced color \
    font ',14' linewidth 1 
set key left top
set title "Fallos de lectura en caché"
set output "cache_lectura.eps"
set encoding utf8

plot "cache_1024.dat" using 1:2 with linespoints title "./slow 1024", \
     "cache_1024.dat" using 1:4 with linespoints title "./fast 1024", \
     "cache_2048.dat" using 1:2 with linespoints title "./slow 2048", \
     "cache_2048.dat" using 1:4 with linespoints title "./fast 2048", \
     "cache_4096.dat" using 1:2 with linespoints title "./slow 4096", \
     "cache_4096.dat" using 1:4 with linespoints title "./fast 4096", \
     "cache_8192.dat" using 1:2 with linespoints title "./slow 8192", \
     "cache_8192.dat" using 1:4 with linespoints title "./fast 8192"

set terminal pngcairo enhanced font "Liberation Sans,10"
set output "cache_lectura.png"
replot

set ylabel "Número de fallos de escritura"
set title "Fallos de escritura en caché"
set output "cache_escritura.eps"

set terminal postscript eps size 5,3.5 enhanced color \
    font ',14' linewidth 1 

plot "cache_1024.dat" using 1:3 with linespoints title "./slow 1024", \
     "cache_1024.dat" using 1:5 with linespoints title "./fast 1024", \
     "cache_2048.dat" using 1:3 with linespoints title "./slow 2048", \
     "cache_2048.dat" using 1:5 with linespoints title "./fast 2048", \
     "cache_4096.dat" using 1:3 with linespoints title "./slow 4096", \
     "cache_4096.dat" using 1:5 with linespoints title "./fast 4096", \
     "cache_8192.dat" using 1:3 with linespoints title "./slow 8192", \
     "cache_8192.dat" using 1:5 with linespoints title "./fast 8192"

set terminal pngcairo enhanced font "Liberation Sans,10"
set output "cache_escritura.png"
replot

quit
