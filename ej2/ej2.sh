#!/bin/bash


P=$((1318%10))


nveces=3

MIN=$((2000+1024*$P))
MAX=$((2000+1024*($P+1)))

#MIN=0
#MAX=1024


rm -f *.tmp *.dat


# hacer esos incrementos de 64 n veces
for (( i=0; i<nveces; i++))
do
	#hacer pruebas con incremento de tamaño de 64 en 64
	for (( N=MIN; N<=$MAX; N=N+128))
	do

		#Variamos el tamaño de las caches (1024,2048,4096,8192)
        for(( c_size = 1024; c_size <= 8192; c_size += c_size ))
        do
            
            echo $c_size

            # Sacamos los tiempos de manera intercalada 
            # para el tamaño N y para N+64

            valgrind --tool=cachegrind --cachegrind-out-file=$c_size"_"$N"_slow.txt" --I1=$c_size,1,64 --D1=$c_size,1,64 --LL=8388608,1,64 ./slow $N
            valgrind --tool=cachegrind --cachegrind-out-file=$c_size"_"$((N+64))"_slow.txt" --I1=$c_size,1,64 --D1=$c_size,1,64 --LL=8388608,1,64 ./slow $((N+64))
            valgrind --tool=cachegrind --cachegrind-out-file=$c_size"_"$N"_fast.txt" --I1=$c_size,1,64 --D1=$c_size,1,64 --LL=8388608,1,64 ./fast $N
            valgrind --tool=cachegrind --cachegrind-out-file=$c_size"_"$((N+64))"_fast.txt" --I1=$c_size,1,64 --D1=$c_size,1,64 --LL=8388608,1,64 ./fast $((N+64))
            #n1_fast="$(./fast $N1 | grep "Execution time:" | awk '{print $3}')"

            n1_slow=$(cg_annotate $c_size"_"$N"_slow.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)
            n2_slow=$(cg_annotate $c_size"_"$((N+64))"_slow.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)
            n1_fast=$(cg_annotate $c_size"_"$N"_fast.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)
            n2_fast=$(cg_annotate $c_size"_"$((N+64))"_fast.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)

            echo $N" "$n1_slow" "$n1_fast >> $c_size.tmp
            echo $((N+64))" "$n2_slow" "$n2_fast >> $c_size.tmp

            #falta hacer medias
		done

	done

done

rm *.txt
#agrupamos valores por primera columna y hallamos la media

awk '{
        slow_r[$1]   += $2;
        slow_w[$1]   += $3;
        fast_r[$1]   += $4;
        fast_w[$1]   += $5;
        sum[$1]      += 1;
    }
    END{
        for (t in slow_r) {
            print t " " slow_r[t]/sum[t] " " slow_w[t]/sum[t] " " fast_r[t]/sum[t] " " fast_w[t]/sum[t] >> "cache_1024.dat";
        }
    }' 1024.tmp


awk '{
        slow_r[$1]   += $2;
        slow_w[$1]   += $3;
        fast_r[$1]   += $4;
        fast_w[$1]   += $5;
        sum[$1]      += 1;
    }
    END{
        for (t in slow_r) {
            print t " " slow_r[t]/sum[t] " " slow_w[t]/sum[t] " " fast_r[t]/sum[t] " " fast_w[t]/sum[t] >> "cache_2048.dat";
        }
    }' 2048.tmp

awk '{
        slow_r[$1]   += $2;
        slow_w[$1]   += $3;
        fast_r[$1]   += $4;
        fast_w[$1]   += $5;
        sum[$1]      += 1;
    }
    END{
        for (t in slow_r) {
            print t " " slow_r[t]/sum[t] " " slow_w[t]/sum[t] " " fast_r[t]/sum[t] " " fast_w[t]/sum[t] >> "cache_4096.dat";
        }
    }' 4096.tmp

awk '{
        slow_r[$1]   += $2;
        slow_w[$1]   += $3;
        fast_r[$1]   += $4;
        fast_w[$1]   += $5;
        sum[$1]      += 1;
    }
    END{
        for (t in slow_r) {
            print t " " slow_r[t]/sum[t] " " slow_w[t]/sum[t] " " fast_r[t]/sum[t] " " fast_w[t]/sum[t] >> "cache_8192.dat";
        }
    }' 8192.tmp

sort cache_1024.dat -o cache_1024.dat
sort cache_2048.dat -o cache_2048.dat
sort cache_4096.dat -o cache_4096.dat
sort cache_8192.dat -o cache_8192.dat

echo "Generating plot..."
gnuplot << END_GNUPLOT
set grid

set xlabel "Tamaño Matriz"
set ylabel "Fallos de lectura"

set terminal pngcairo enhanced font "Liberation Sans,10"

set title "Fallos cache lectura"
set output "cache_lectura.png"

plot "cache_1024.dat" using 1:2 with linespoints title "./slow 1024", \
     "cache_1024.dat" using 1:4 with linespoints title "./fast 1024", \
     "cache_2048.dat" using 1:2 with linespoints title "./slow 2048", \
     "cache_2048.dat" using 1:4 with linespoints title "./fast 2048", \
     "cache_4096.dat" using 1:2 with linespoints title "./slow 4096", \
     "cache_4096.dat" using 1:4 with linespoints title "./fast 4096", \
     "cache_8192.dat" using 1:2 with linespoints title "./slow 8192", \
     "cache_8192.dat" using 1:4 with linespoints title "./fast 8192"

set ylabel "Fallos de escritura"
set title "Fallos cache escritura"
set output "cache_escritura.png"

plot "cache_1024.dat" using 1:3 with linespoints title "./slow 1024", \
     "cache_1024.dat" using 1:5 with linespoints title "./fast 1024", \
     "cache_2048.dat" using 1:3 with linespoints title "./slow 2048", \
     "cache_2048.dat" using 1:5 with linespoints title "./fast 2048", \
     "cache_4096.dat" using 1:3 with linespoints title "./slow 4096", \
     "cache_4096.dat" using 1:5 with linespoints title "./fast 4096", \
     "cache_8192.dat" using 1:3 with linespoints title "./slow 8192", \
     "cache_8192.dat" using 1:5 with linespoints title "./fast 8192"

quit
END_GNUPLOT

rm *.tmp

#sort ej1.txt -o ej1.txt

echo $P;