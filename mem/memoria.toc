\select@language {spanish}
\contentsline {section}{Ejercicio 0: Informaci\IeC {\'o}n sobre la cach\IeC {\'e} del sistema}{3}{section*.2}
\contentsline {paragraph}{}{3}{section*.3}
\contentsline {section}{Ejercicio 1: Memoria cach\IeC {\'e} y rendimiento}{5}{section*.4}
\contentsline {paragraph}{}{5}{section*.5}
\contentsline {paragraph}{}{5}{section*.6}
\contentsline {paragraph}{}{5}{section*.7}
\contentsline {paragraph}{Indique una explicaci\IeC {\'o}n razonada del motivo por el que hay que realizar m\IeC {\'u}ltiples veces la toma de medidas de rendimiento para cada programa y tama\IeC {\~n}o de matriz.}{6}{section*.8}
\contentsline {paragraph}{}{6}{section*.9}
\contentsline {section}{Ejercicio 2: Tama\IeC {\~n}o de la cach\IeC {\'e} y rendimiento}{7}{section*.10}
\contentsline {paragraph}{Justifique el efecto observado. \IeC {\textquestiondown }Se observan cambios de tendencia al variar los tama\IeC {\~n}os de cach\IeC {\'e} para el programa slow?}{7}{section*.11}
\contentsline {paragraph}{}{7}{section*.12}
\contentsline {paragraph}{\IeC {\textquestiondown }Y para el programa fast?}{8}{section*.13}
\contentsline {paragraph}{}{8}{section*.14}
\contentsline {paragraph}{\IeC {\textquestiondown }Var\IeC {\'\i }a la tendencia cuando se fija en un tama\IeC {\~n}o de cach\IeC {\'e} concreto y compara el programa slow y fast? \IeC {\textquestiondown }A qu\IeC {\'e} se debe cada uno de los efectos observados?}{8}{section*.15}
\contentsline {paragraph}{}{8}{section*.16}
\contentsline {section}{Ejercicio 3: Cach\IeC {\'e} y multiplicaci\IeC {\'o}n de matrices}{9}{section*.17}
\contentsline {paragraph}{}{9}{section*.18}
\contentsline {paragraph}{}{9}{section*.19}
\contentsline {paragraph}{}{10}{section*.20}
\contentsline {paragraph}{}{10}{section*.21}
