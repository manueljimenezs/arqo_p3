#!/usr/bin/gnuplot
#set grid

set xlabel "Tamaño de matriz"
set ylabel "Tiempo de ejecución (s)"

set terminal pngcairo enhanced font "Liberation Sans,10"

set title "Tiempo de ejecución multiplicación (matriz normal-traspuesta)"
set output "mult_time.png"

set encoding "utf8"
set terminal pngcairo enhanced font "Liberation Sans,10"


plot "time_norm_trasp.dat" using 1:2 with lines title "./norm", \
	 "time_norm_trasp.dat" using 1:3 with lines title "./trasp"

set terminal postscript eps size 5,3.5 enhanced color \
    font ',14' linewidth 1 
set output "mult_time.eps"

replot

quit
