#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "arqo3.h"

tipo **compute(tipo **mat1, tipo **mat2, tipo **matres, int n);
tipo **generateTrasp(tipo **mat1, int n);

int main( int argc, char *argv[])
{
	int n;
	tipo **m1=NULL;
	tipo **m2=NULL;
	tipo **matres=NULL;
	tipo **aux = NULL;
	struct timeval fin,ini;

	int i,j;

	printf("Word size: %ld bits\n",8*sizeof(tipo));

	if( argc!=2 )
	{
		printf("Error: ./%s <matrix size>\n", argv[0]);
		return -1;
	}
	n=atoi(argv[1]);

	m1=generateMatrix(n);
	m2=generateMatrix(n);
	matres=generateEmptyMatrix(n);
	aux=generateEmptyMatrix(n);

	if( !m1 || !m2 || !matres || !aux)
	{
		return -1;
	}

	/* Pasar los datos a la matriz auxiliar que se utiliza para trasponer */
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			aux[i][j] = m2[i][j];
		}
	}

	gettimeofday(&ini,NULL);

	/* Trasponer la matriz m2 (utilizando la matriz auxiliar que tiene los mismos datos que m2) */
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			m2[i][j] = aux[j][i];
		}
	}

	/* Main computation */
	compute(m1,m2,matres,n);
	/* End of computation */

	gettimeofday(&fin,NULL);
	printf("Execution time: %f\n", ((fin.tv_sec*1000000+fin.tv_usec)-(ini.tv_sec*1000000+ini.tv_usec))*1.0/1000000.0);
	/*printf("Total: %lf\n",res);*/
	
	free(m1);
	free(m2);
	free(matres);

	return 0;
}



tipo **compute(tipo **mat1, tipo **mat2, tipo **matres, int n)
{
	int i,j,k;
	
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			for(k=0;k<n;k++){
				/* Ya esta hecha la traspuesta asi que se accede normal a la matriz [j][k] en lugar de [k][j] */
				matres[i][j] += mat1[i][k]*mat2[j][k];
			}
		}
	}
	return matres;
}
