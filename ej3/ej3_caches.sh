#!/bin/bash

# P = (num_grupo + num_pareja) mod 10
P=$((1318%2))
#P=8

nveces=1

MIN=$((256+256*$P))
MAX=$((256+256*($P+1)))

#MIN=2304
#MAX=2560


rm -f *.tmp *.dat


# hacer esos incrementos de 64 n veces
for (( i=0; i<nveces; i++))
do
	#hacer pruebas con incremento de tamaño de 64 en 64
	for (( N=MIN; N<=$MAX; N=N+32))
	do
        # Sacamos los tiempos de manera intercalada 
        # para el tamaño N y para N+16

        valgrind --tool=cachegrind --cachegrind-out-file="cache_"$N"_norm.txt" ./norm $N
        valgrind --tool=cachegrind --cachegrind-out-file="cache_"$((N+16))"_norm.txt" ./norm $((N+16))
        valgrind --tool=cachegrind --cachegrind-out-file="cache_"$N"_trasp.txt" ./trasp $N
        valgrind --tool=cachegrind --cachegrind-out-file="cache_"$((N+16))"_trasp.txt" ./trasp $((N+16))

        n1_norm=$(cg_annotate "cache_"$N"_norm.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)
        n2_norm=$(cg_annotate "cache_"$((N+16))"_norm.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)
        n1_trasp=$(cg_annotate "cache_"$N"_trasp.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)
        n2_trasp=$(cg_annotate "cache_"$((N+16))"_trasp.txt" | grep "PROGRAM TOTALS" | cut -f 5,8 -d " " | tr --delete ,)

        echo $N" "$n1_norm" "$n1_trasp >> cache.tmp
        echo $((N+16))" "$n2_norm" "$n2_trasp >> cache.tmp

	done

done

rm *.txt

# Agrupamos valores por primera columna y hallamos la media

awk '{
        norm_r[$1]   += $2;
        norm_w[$1]   += $3;
        trasp_r[$1]   += $4;
        trasp_w[$1]   += $5;
        sum[$1]      += 1;
    }
    END{
        for (t in norm_r) {
            print t " " norm_r[t]/sum[t] " " norm_w[t]/sum[t] " " trasp_r[t]/sum[t] " " trasp_w[t]/sum[t] >> "cache.dat";
        }
    }' cache.tmp


sort cache.dat -o cache.dat


echo "Generating plot..."
gnuplot << END_GNUPLOT
set grid

set xlabel "Tamaño Matriz"
set ylabel "Fallos de lectura"

set terminal pngcairo enhanced font "Liberation Sans,10"

set title "Fallos cache Mult. matriz (normal-traspuesta)"
set output "mult_cache.png"

plot "cache.dat" using 1:2 with linespoints title "lectura ./norm", \
     "cache.dat" using 1:4 with linespoints title "lectura ./trasp", \
plot "cache.dat" using 1:3 with linespoints title "escritura ./norm", \
     "cache.dat" using 1:5 with linespoints title "escritura ./trasp"
quit
END_GNUPLOT

rm *.tmp

#sort ej1.txt -o ej1.txt

echo $P;
