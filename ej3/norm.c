#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "arqo3.h"

tipo **compute(tipo **mat1, tipo **mat2, tipo **matres, int n);

int main( int argc, char *argv[])
{
	int n;
	tipo **m1=NULL;
	tipo **m2=NULL;
	tipo **matres=NULL;
	struct timeval fin,ini;

	printf("Word size: %ld bits\n",8*sizeof(tipo));

	if( argc!=2 )
	{
		printf("Error: ./%s <matrix size>\n", argv[0]);
		return -1;
	}
	n=atoi(argv[1]);

	m1=generateMatrix(n);
	m2=generateMatrix(n);
	matres=generateEmptyMatrix(n);

	if( !m1 || !m2 || !matres )
	{
		return -1;
	}


	
	gettimeofday(&ini,NULL);

	/* Main computation */
	compute(m1,m2,matres,n);
	/* End of computation */

	gettimeofday(&fin,NULL);
	printf("Execution time: %f\n", ((fin.tv_sec*1000000+fin.tv_usec)-(ini.tv_sec*1000000+ini.tv_usec))*1.0/1000000.0);
	/*printf("Total: %lf\n",res);*/

	free(m1);
	free(m2);
	free(matres);

	return 0;
}


tipo **compute(tipo **mat1, tipo **mat2, tipo **matres, int n)
{
	int i,j,k;
	
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			for(k=0;k<n;k++){
				matres[i][j] += mat1[i][k]*mat2[k][j];
			}
		}
	}
	return matres;
}
