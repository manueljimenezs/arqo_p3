#!/usr/bin/gnuplot

#set grid

set xlabel "Tamaño de matriz"
set ylabel "Número de fallos"

#set terminal pngcairo enhanced font "Liberation Sans,10"
set terminal postscript eps size 5,3.5 enhanced color \
    font ',14' linewidth 1 
set key left top
set title "Fallos cache Mult. matriz (normal-traspuesta)"
set output "mult_cache.eps"
set encoding utf8

plot "mult.dat" using 1:3 with linespoints title "lectura ./norm", \
     "mult.dat" using 1:6 with linespoints title "lectura ./trasp", \
     "mult.dat" using 1:4 with linespoints title "escritura ./norm", \
     "mult.dat" using 1:7 with linespoints title "escritura ./trasp"

set terminal pngcairo enhanced font "Liberation Sans,10"
set output "mult_cache.png"
replot

quit
