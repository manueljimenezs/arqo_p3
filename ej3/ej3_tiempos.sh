#!/bin/bash


P=0
nveces=3

MIN=$((256+256*$P))
MAX=$((256+256*($P+1)))

rm -f temp time_norm_trasp.dat

for (( n=0; n<nveces; n++))
do
	N=$MIN

	while (( $N<=$MAX ))
	do
		# Sacamos los tiempos de manera intercalada 
		# para el tamaño N y para N+16

		echo -n "S "$N" "
		n1_norm="$(./norm $N | grep "Execution time:" | awk '{print $3}')"
		echo -n "S "$((N+16))" "
		n2_norm="$(./norm $((N+16)) | grep "Execution time:" | awk '{print $3}')"
		echo -n "F "$N" "
		n1_trasp="$(./trasp $N | grep "Execution time:" | awk '{print $3}')"
		echo -n "F "$((N+16))" "
		n2_trasp="$(./trasp $((N+16)) | grep "Execution time:" | awk '{print $3}')"
		
		# Imprimimos los datos ordenados en el fichero de salida 
		echo $N" "$n1_norm" "$n1_trasp >> temp
		echo $((N+16))" "$n2_norm" "$n2_trasp >> temp

		N=$(($N+32))

	done

done

# Agrupamos valores por primera columna y hallamos la media

awk '{
        norm[$1]   += $2;
        trasp[$1]   += $3;
        sum[$1]    += 1;
    }
    END{
        for (t in norm) {
            print t " " norm[t]/sum[t] " " trasp[t]/sum[t] >> "time_norm_trasp.dat";
        }
    }' temp

rm temp

sort time_norm_trasp.dat -o time_norm_trasp.dat

echo "Generating plot..."
gnuplot << END_GNUPLOT
set grid

set xlabel "Matrix Size"
set ylabel "Execution time (s)"

set terminal pngcairo enhanced font "Liberation Sans,10"

set title "Tiempo de ejecución multiplicación (matriz normal-traspuesta)"
set output "mult_time.png"

plot "mult.dat" using 1:2 with lines title "./norm", \
	 "mult.dat" using 1:5 with lines title "./trasp"
quit
END_GNUPLOT

echo $P;
